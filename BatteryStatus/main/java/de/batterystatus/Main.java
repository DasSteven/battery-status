package de.batterystatus;

import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.batterystatus.module.BatteryModule;
import de.batterystatus.module.BatteryTextModule;
import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.BooleanElement;
import net.labymod.settings.elements.ControlElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.utils.Consumer;
import net.labymod.utils.Material;

public class Main extends LabyModAddon {
	
	public static String ninety;
	public static String eighty;
	public static String seventy;
	public static String sixty;
	public static String fifty;
	public static String fourty;
	public static String thirty;
	public static String twenty;
	public static String ten;
	public static String low;
	
	public static boolean enabled = true;
	
	@Override
	public void onEnable() {
		ninety = new String("batterystatus/ninety.png");
		eighty = new String("batterystatus/eighty.png");
		seventy = new String("batterystatus/seventy.png");
		sixty = new String("batterystatus/sixty.png");
		fifty = new String("batterystatus/fifty.png");
		fourty = new String("batterystatus/fourty.png");
		thirty = new String("batterystatus/thirty.png");
		twenty = new String("batterystatus/twenty.png");
		ten = new String("batterystatus/ten.png");
		low = new String("batterystatus/low.png");
		getApi().registerModule(new BatteryModule((short) 0, (short) 0, (short) 0, (short) 0, (short) 0, (short) 0));
		getApi().registerModule(new BatteryTextModule());
	}
	
	@Override
	protected void fillSettings(List<SettingsElement> list) {
		BooleanElement booleanElement = new BooleanElement("Enabled", new ControlElement.IconData(Material.LEVER), new Consumer<Boolean>() {
			
			@Override
			public void accept(Boolean change) {
				enabled = change;
			}
			
		}, false);
		list.add(booleanElement);
	}
	
	@Override
	public void loadConfig() {
		
	}
	
}
