package de.batterystatus.module;

import de.batterystatus.util.Kernel32;
import net.labymod.ingamegui.moduletypes.SimpleModule;
import net.labymod.settings.elements.ControlElement.IconData;
import net.minecraft.util.ResourceLocation;

public class BatteryTextModule extends SimpleModule {
	
	@Override
	public String getDefaultValue() {
		return "0%";
	}

	@Override
	public String getDisplayName() {
		return "Battery Level";
	}

	@Override
	public String getDisplayValue() {
		Kernel32.SYSTEM_POWER_STATUS batteryStatus = new Kernel32.SYSTEM_POWER_STATUS();
		String batteryString = batteryStatus.getBatteryLifePercent();
		if(Integer.valueOf(batteryString.replaceAll("%", "")) == 0) {
			return "No Battery found";
		}
		return batteryString;
	}

	@Override
	public String getDescription() {
		return "Shows your Battery Level as a Number";
	}

	@Override
	public IconData getIconData() {
		return new IconData(new ResourceLocation("batterystatus/iconmodule2.png"));
	}

	@Override
	public String getSettingName() {
		return "Battery Status as a Number";
	}
	
	@Override
	public String getControlName() {
		return "Battery Status as a Number";
	}
	
	@Override
	public int getSortingId() {
		return 0;
	}

	@Override
	public void loadSettings() {
		
	}

}
