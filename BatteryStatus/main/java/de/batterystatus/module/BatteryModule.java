package de.batterystatus.module;

import de.batterystatus.Main;
import de.batterystatus.util.Kernel32;
import net.labymod.ingamegui.Module;
import net.labymod.ingamegui.moduletypes.ResizeableModule;
import net.labymod.ingamegui.moduletypes.SimpleModule;
import net.labymod.settings.elements.ControlElement.IconData;
import net.labymod.utils.ModColor;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.network.play.server.S3FPacketCustomPayload;
import net.minecraft.util.ResourceLocation;

public class BatteryModule extends Module {
	
	public static BatteryModule instance;
	
	@Override
	public void draw(int x, int y, int rightX) {
		ResourceLocation loc = null;
		Kernel32.SYSTEM_POWER_STATUS batteryStatus = new Kernel32.SYSTEM_POWER_STATUS();
		String batteryString = batteryStatus.getBatteryLifePercent().replaceAll("%", "");
		int battery = Integer.parseInt(batteryString);
		if(battery > 89) {
			loc = new ResourceLocation(Main.ninety);
		}
		else if(battery > 79) {
			loc = new ResourceLocation(Main.eighty);
		}
		else if(battery > 69) {
			loc = new ResourceLocation(Main.seventy);
		}
		else if(battery > 59) {
			loc = new ResourceLocation(Main.sixty);
		}
		else if(battery > 49) {
			loc = new ResourceLocation(Main.fifty);
		}
		else if(battery > 39) {
			loc = new ResourceLocation(Main.fourty);
		}
		else if(battery > 29) {
			loc = new ResourceLocation(Main.thirty);
		}
		else if(battery > 19) {
			loc = new ResourceLocation(Main.twenty);
		}
		else if(battery > 9) {
			loc = new ResourceLocation(Main.ten);
		}
		else {
			loc = new ResourceLocation(Main.low);
		}
		mc.getTextureManager().bindTexture(loc);
		if(Integer.valueOf(batteryString.replaceAll("%", "")) == 0) {
			mc.getTextureManager().bindTexture(new ResourceLocation("batterystatus/nobattery.png"));
		}
		Gui.drawModalRectWithCustomSizedTexture(x, y, (float) getHeight() + 25, (float) getWidth(), 50, 25, 50, 25);
//		getCurrentModuleGui().drawString(fontRendererIn, text, rightX, y, color);
		super.draw(x, y, rightX);
	}
	
	public BatteryModule(short defaultWidth, short defaultHeight, short minWidth, short minHeight, short maxWidth,
			short maxHeight) {
		super();
		instance = this;
	}
	@Override
	public String getDescription() {
		return "";
	}
	
	@Override
	public double getHeight() {
		return 25;
	}
	
	@Override
	public IconData getIconData() {
		IconData icon = null;
		Kernel32.SYSTEM_POWER_STATUS batteryStatus = new Kernel32.SYSTEM_POWER_STATUS();
		String batteryString = batteryStatus.getBatteryLifePercent().replaceAll("%", "");
		int battery = Integer.parseInt(batteryString);
		if(battery > 89) {
			icon = new IconData(Main.ninety);
		}
		else if(battery > 79) {
			icon = new IconData(Main.eighty);
		}
		else if(battery > 69) {
			icon = new IconData(Main.seventy);
		}
		else if(battery > 59) {
			icon = new IconData(Main.sixty);
		}
		else if(battery > 49) {
			icon = new IconData(Main.fifty);
		}
		else if(battery > 39) {
			icon = new IconData(Main.fourty);
		}
		else if(battery > 29) {
			icon = new IconData(Main.thirty);
		}
		else if(battery > 19) {
			icon = new IconData(Main.twenty);
		}
		else if(battery > 9) {
			icon = new IconData(Main.ten);
		}
		else {
			icon = new IconData(Main.low);
		}
		return new IconData(new ResourceLocation("batterystatus/iconmodule.png"));
	}
	
	@Override
	public String getSettingName() {
		return "Battery Status as Image";
	}
	
	@Override
	public int getSortingId() {
		return 0;
	}
	
	@Override
	public double getWidth() {
		return 50;
	}
	
	@Override
	public void loadSettings() {
		
	}
	
	@Override
	public String getControlName() {
		return "Battery Status as Image";
	}
	
}
